import { useTranslation } from "react-i18next"
import { Text } from "../lib/text"
import Search from "./_functional/search/Search"
import HeaderOfFB from "./_main/FeatureBook/HeaderOfFB"
import BodyofFeatured from "./_main/FeatureBook/BodyOfFB"
import SearchResult from "./_main/SearchResult/SearchResult"
 
 
import { Link, useLocation, useSearchParams } from "react-router-dom"
import { TSort } from "./_functional/search/api"
import { CarouselTransition } from "./_functional/Slider/BookCardSlider"
import HeaderForMP from "./_main/FeatureBook/HeaderForMP"
 
export const Loader = () => 'Route loader'
export const Action = () => 'Route action'
export const Catch = () => <div>Route errorrrrrr</div>
 
 
 
export default function Home() {
  
    const { t} = useTranslation('home')
    const [q] = useSearchParams()
    const location = useLocation()
    const Content :TSort[] = ["rating","readinglog","new","random"]
  const handleRender = () =>{

    let  text = q.get('q') 
    if(text==='' || !text) return <CarouselTransition>
               {
                  Content.map((item)=>{
                    return  <div key={"main_page_1"+item} className="   h-full w-full flex flex-col items-center px-14 gap-2 ">
                    <HeaderForMP type={item}></HeaderForMP>
                    <BodyofFeatured limit={1} type={item}></BodyofFeatured>
                </div>
                  })
                }
    </CarouselTransition>  
 
    return (<>
            <div className=" w-full flex justify-between">
                      
                        <Link    to={`/search${location?.search}`}>
                        <Text._14_400 color=" " className=" text-yellow  cursor-pointer ">
                           View more -/ <span className=" italic">{text}</span> /- <span className=" underline underline-offset-1">here</span>  
                        </Text._14_400>
                        </Link>      
                      </div>                        
                       <SearchResult></SearchResult> 
    </>)
  }
  
  return <div   className=" w-full flex flex-col items-center pb-[100px] justify-center gap-[100px] tab:gap-[70px] mb:gap-[50px] relative    ">
                <div className=" w-full flex    mb:flex-col  min-h-screen gap-5 ">
                    <div className="pt-36 px-8  mb:hidden mb:w-full justify-center gap-6 w-[50%] flex flex-col  ">
                            <Text._64_500 className="  w-full leading-[0.9]">
                              {t('caption')}
                            </Text._64_500>
                            <Text.P_22_300 className=" w-full "> 
                              {t('quote')}
                            </Text.P_22_300>
                            <Search  page="mainpage"></Search>
                    </div>
                    <div className="px-8 pt-24  mb:w-full flex h-full flex-col pb-[50px] gap-3 bg-pink-300 w-[50%]"> 
                        <div className=" w-full lap:hidden">
                        <Search  page="mainpage"></Search>
                        </div>
                        {handleRender()}
                    </div>
                </div>
                
                {
                  Content.map((item)=>{
                    return  <div key={"main_page"+item} className=" gap-[100px] tab:gap-[70px] mb:gap-[50px] min-h-screen w-full flex flex-col items-center px-8">
                    <HeaderOfFB type={item}></HeaderOfFB>
              
                    <BodyofFeatured limit={8} type={item}></BodyofFeatured>
                
                    
                </div>
                  })
                }
             

  </div >   
}