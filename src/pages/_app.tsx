import { Outlet } from 'react-router-dom';

import { Modals } from '../routes';

import './App.css'
 
 
import { QueryClient, QueryClientProvider } from "react-query";
import Navbar from './_main/navbar/nabar';
 
 

const queryClient = new QueryClient({});
export default function App() {
 
 
 
  return (
    <>
       <QueryClientProvider client={queryClient}>
      <main    className={` main  min-h-screen duration-300 ease-in-out flex flex-col items-center w-full` }>
        <Navbar></Navbar>
        
        <Outlet />
    
      </main>

      <Modals />
      </QueryClientProvider>
    </>
  );
}
