import {  useEffect, useLayoutEffect, useRef } from "react"
 
import gsap from "gsap"
import { ScrollTrigger } from "gsap/ScrollTrigger"
import { useSearchStore } from "../_functional/search/api.store"
import { useSearchParams } from "react-router-dom"
import { RenderSearchBook } from "../_functional/handleRender/Searchbook"
import Search from "../_functional/search/Search"
 

export default function SearchPage(){

   
 
 
    const main = useRef(null)
 
    const  isLoading = useSearchStore((state)=> state.isLoading)
    const [q,setQ] = useSearchParams()
    useEffect(()=>{
        setQ(prev =>{
            prev.set('limit',"25")
            prev.set('page',"1")
            prev.set('sort',"rating")
            return prev
        },{ replace:true})
    },[])
   
    useLayoutEffect(() => {
        function show(batch:any) {
            gsap.set(batch, {opacity: 0, rotateX:30, rotateZ:30,  rotateY:70, y:100, });
            
            gsap.to(batch, {opacity: 1,y:0, rotateX:0,rotateZ:0  , rotateY:0 , stagger: 0.3, overwrite: true, duration: 0.75});
          }
          function hide(batch:any) {
            gsap.set(batch, {opacity: 0, rotateX:30, rotateZ:30, rotateY:70, y:100, overwrite: true});
          }
        const ctx = gsap.context(() => {
                ScrollTrigger.batch(".card",{
                    //interval: 1,
                   // batchMax:2,
                   onEnter: show,
                   onLeave: hide,
                   onEnterBack: show,
                   onLeaveBack: hide
                })
        }, main);

        return ()=>{
            ctx.kill()
        }
      },[isLoading]);
 
 
    return <div ref={main} id={q.toString()} className=" pt-36 w-full px-8 flex-col flex gap-20 min-h-screen  ">
       
     
         
            <div className=" w-full"><Search  page="diff"></Search></div>

            
          <div className=" w-full grid gap-4 gap-y-8 grid-cols-4">
                  {RenderSearchBook()}            
            </div> 
           
    </div>
}