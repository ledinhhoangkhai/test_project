 
import { create } from 'zustand'
import { TBooksResponse } from './type.api'

 
export type LoadingState ={
    isLoadng: true
} 

export type SuccessBookState ={
    isLoadng: false,
    data: TBooksResponse
} 
export type ErrorBookState ={
    isLoadng: false,
    error : { message:string}
} 

export type BookState = 
|LoadingState
|SuccessBookState
| ErrorBookState

export type StoreState ={
    data : TBooksResponse | null,
    isLoading: Boolean,
    success:(data:TBooksResponse) => void,
    loading: (i: Boolean) => void
}
export const useSearchStore = create<StoreState>((set)=>({
    data:null,
    isLoading:false,
    success: (data:TBooksResponse)=> set((state)=>({...state,data:data, isLoading:false })),
    loading: (isLoading:Boolean)=> set((state)=>({...state, isLoading:isLoading})),
  
}))