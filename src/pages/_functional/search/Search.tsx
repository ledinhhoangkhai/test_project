 
import {     TSort, useBooksSearch } from "./api"
import {   useNavigate, useSearchParams } from "react-router-dom"
import { useSearchStore } from "./api.store"
import {   useDebounceSearch } from "../../../lib/hooks/useDeBounced"
 
import {   TParams } from "./type.api"
import { DefaultPagination } from "../../../lib/pagination/Pagination"
import { MagnifyingGlassIcon } from "@heroicons/react/24/outline"
 
import { Select, Option } from "@material-tailwind/react";
 


  
export default function Search({page}:{
    page : 'mainpage' | 'diff'
}){
    const [q,setQ]= useSearchParams({ 
        q:"",
        limit: page ==="mainpage" ? "2" : "25",
        page:"1",
        sort:"",
        
    })
    
    const navigate = useNavigate()
    const {  success ,loading } = useSearchStore()
   
    const params:TParams = {
        text:  q.get('q')|| "",
        limit: page ==="mainpage" ? "2" : q.get('limit'),
        page: q.get('page') || "1",
        sort : q.get('sort') ||  "rating",
    }
    const debounceVal = useDebounceSearch(params,500)
    const { isLoading  } = useBooksSearch('querystring',{
        ...debounceVal,
            success:success,
            start: ()=> loading(true)
    })
        
    const SortItem: TSort[]  =["new","random","rating","readinglog"]
    const LimitItem: string[]  =["25","50","100" ]
   
    return <div className=" text-22   w-full sticky top-9  flex gap-12 items-center mb:flex-col mb:justify-start mb:items-start  justify-end">
              <form onSubmit={()=>{ 
                
                navigate(`/search?q=${params.text}&limit=${params.limit}&sort=${params.sort}`)
                
                }} className=" self-start  w-full flex items-center gap-2   ">
             <MagnifyingGlassIcon className=" pl-4 absolute" height={40} width={40}></MagnifyingGlassIcon>
            <input onChange={ e => setQ( prev =>{
            prev.set('q',e.target.value )
            return prev
         },{replace:true})} id="q"  placeholder="Search any book here !!"  disabled={isLoading} value={params.text || ''}  type="text" className={`${isLoading ? " animate-pulse opacity-70 ": ""} w-[90%] tab:text-18 mb:text-12px   h-10 rounded-[40rem] px-12 border-2 border-black focus:ring-offset-0 focus:border-0 focus:ring-0 `}></input>
            <input className="text-22 tab:text-16px mb:text-14 border px-2 py-2 rounded-[40rem] border-black" disabled={isLoading} type="submit"></input>
            <div>
                
        
            </div>
            </form >
             { page==="diff" && <div className=" gap-12 flex w-[40%] mb:w-full mb:items-center flex-col"> 
                       <div className=" flex mb:w-full mb:flex-col  tab:flex-row w-1/2 gap-5">

                    <Select disabled={isLoading} variant="static"   value={params.sort|| undefined}
                        label="Select Sort"  className="mb:min-w-[100px]  min-h-fit  flex  text-18  z-[999]">
                                <Option   key={''} value={undefined}>{"None"}</Option>
                                {SortItem.map((item:TSort)=>{
                                return <Option onClick={ ()=>setQ( prev =>{
                                    prev.set('sort',item)       
                                    return prev
                                    },{replace:true})} key={item}  value={item}>{item}</Option>
                            })}
                    </Select>
                     
                    <Select disabled={isLoading}  variant="static"  value={params.limit|| undefined}
                        label="Select Limit"  className="mb:min-w-[100px] min-h-fit flex  text-18 z-[999]">
                                {LimitItem.map((item)=>{
                                return <Option onClick={ ()=>setQ( prev =>{
                                    prev.set('limit',item)       
                                    return prev
                                    },{replace:true})} key={item}  value={item}>{item}</Option>
                            })}
                    </Select>  
                       </div>
                    <DefaultPagination isLoading={isLoading} ></DefaultPagination>  
             </div>}
           
                 

              
    </div>
}
 
