import { TSort } from "./api"


export type Docs ={
    already_read_count :number
    author_alternative_name:  string[]
    author_facet:string[]
    author_key:string[]
    author_name:string[]
    contributor:string[]
    cover_edition_key:string
    cover_i:number | null
    currently_reading_count:number
    ebook_count_i:number
    edition_count:number
    edition_key:string[]
    first_publish_year:number
    first_sentence:string[]
    isbn:string[]
    key:string
    language:string[] 
    last_modified_i: number
    publish_date:string[]
    lccn:string[] | null
    publish_place:string[]
    publish_year:number[]
    publisher:string[]
    ratings_average: number
    ratings_count:number  
    ratings_count_1:number  
    ratings_count_2:number  
    ratings_count_3:number  
    ratings_count_4:number  
    ratings_count_5:number  
    ratings_sortable:number  
    readinglog_count:number  
    seed:string[]
    subject:string[]
    subject_facet: string[]
    subject_key: string[]
    title: string
    title_sort:string
    title_suggest: string
    type:string
    want_to_read_count: number
}

export type TBooksResponse = {
    numFound  : number
    num_found :number
    offset: null,
    start: number
    docs : Docs[]
}

export type TParams ={
    text:  string | null,
    limit:string | null,
    page: string |null,
    sort : |TSort | null  |string,
}