import {   useQuery } from "react-query"
import { _axios_lib_base, _axios_search, fetchConfig } from "../../../axios_config"
import { TBooksResponse } from "./type.api"
 
 
 


export type TSort =   'new' | 'rating' | 'readinglog' | 'random'   

export const useBooksSearch  = (key:string,{
    text,
    limit=100,
    sort = "rating",
    page =1,
    
    success ,
    start
}:{
    text :string | undefined |null
    limit?: number | string | null,
    sort?:  TSort |null |string,
    page?: number | null | string
    success?: (data:TBooksResponse) => void
    start?: ( ) => void
 
})=>{

 

 return useQuery([`Books_${key}`,{text,limit,sort,page}],{
    queryFn: async ()=>{
       try {
        if(!start){

        }else{
            start()
     
        }
        
        const res = await  _axios_search.get('',{
            params:{
                q:text,
                limit: limit,
                sort: sort,
                has_fulltext:true,
                page
            }})
        return  res.data  as TBooksResponse
       } catch (error) {
            return error 
       }
 
    },
    onSuccess: (data:TBooksResponse)=>{
        if(!success)return
            success(data)
        
    },
    ...fetchConfig
})}
 

 