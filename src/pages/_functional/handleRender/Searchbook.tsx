import { useSearchParams } from "react-router-dom"
import { useSearchStore } from "../search/api.store"
import BookCard, { BookLoading } from "../../../lib/card/BookCard"
import { Text } from "../../../lib/text"
import { Docs } from "../search/type.api"
const emtyItem: Docs = {
    already_read_count: 0,
    author_alternative_name: ["Not found"],
    author_facet: ["Not found"],
    author_key: ["Not found"],
    author_name: ["Not found"],
    contributor: ["Not found"],
    cover_edition_key: "abcdef123456",
    cover_i: null,
    currently_reading_count: 5,
    ebook_count_i: 3,
    edition_count: 2,
    edition_key: ["edition1", "edition2"],
    first_publish_year: 2000,
    first_sentence: ["Once upon a time  Not found :) "],
    isbn: ["Not found"],
    key: "404",
    language: ["English"],
    last_modified_i: 1630000000,
    publish_date: ["Not found"],
    lccn: null,
    publish_place: ["Not found"],
    publish_year: [2021],
    publisher: ["Not found"],
    ratings_average: 4.5,
    ratings_count: 100,
    ratings_count_1: 10,
    ratings_count_2: 20,
    ratings_count_3: 30,
    ratings_count_4: 25,
    ratings_count_5: 15,
    ratings_sortable: 4.2,
    readinglog_count: 50,
    seed: ["seed123"],
    subject: ["Not found", "Not found"],
    subject_facet: ["Fiction", "Mystery"],
    subject_key: ["fiction", "mystery"],
    title: "This book or this page is Not found, please search another one !!! ",
    title_sort: "Not found",
    title_suggest: "Not found",
    type: "book",
    want_to_read_count: 0,
}
export const RenderSearchBook = (type?:"mainpage") =>{
    

    const [q] = useSearchParams()
    const   data   = useSearchStore((state)=> state.data)
    const   isLoading   = useSearchStore((state)=> state.isLoading)
    if(isLoading){
        return <BookLoading  limit={parseInt(q.get('limit')|| "100")} ></BookLoading>
    }
    if(q.get('q')=== "" || q.get('q')=== null ){
      return <> <Text.S_96_500 text="Search your book now !!!" className="   col-span-4 leading-[0.9] flex flex-col">
                 <Text.P_22_300 className=" italic tracking-wide"> Input the any words and </Text.P_22_300>
                 <span className=" typing">
                 Explore the world of books
                 </span>
                
            </Text.S_96_500> 
             
            </> 
    }
    if(!data?.docs.length){
            return <>
            <Text._64_500 className=" col-span-3">
            Not Found For: {q.get('q')}
            </Text._64_500> 
            <BookCard  key={emtyItem.key} className=" card"   item={emtyItem}></BookCard>
            </>
    }
    if(data?.docs.length  < 1){
        return <>
        <Text._64_500 className=" col-span-3">
        Not Found For: {q.get('q')}
        </Text._64_500> 
        <BookCard  key={emtyItem.key} className=" card"   item={emtyItem}></BookCard>
        </> 
    }
     return  data?.docs.map((item:Docs)=>{
                  return <BookCard type={type} key={item.key} className=" card  "   item={item}></BookCard>
    }) 
          
}