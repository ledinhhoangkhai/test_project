 
import {
 
    FacebookIcon,
 
    FacebookShareButton,
 
    LinkedinIcon,
    LinkedinShareButton,
 
    RedditIcon,
    RedditShareButton,
 
    TwitterShareButton,
  
    WeiboIcon,
    WeiboShareButton,
 
    XIcon,
  } from "react-share";
import { LazyImagesUnStyled } from "../../../lib/React-Lazy-Image/LazyImage";
import { parseImageUrl } from "../../../lib/common/getPublicImage";
import Alert from "../../../lib/alert/Alert";
 
export default function Share({uri}:{
  uri:string
}){
 
    const shareUrl =  import.meta.env.VITE_LIB_API_URL+window.location.host + uri 

 
  const handleShare = () =>{
    return navigator.clipboard.writeText(shareUrl);
  }
return  <div className=" pt-1 items-center  w-full  flex justify-between">
        <FacebookShareButton
        className=" self-center"
          url={shareUrl}  >
          <FacebookIcon size={32} round />
        </FacebookShareButton>
        <TwitterShareButton
          url={shareUrl} >
          <XIcon size={32} round />
        </TwitterShareButton>
        <LinkedinShareButton
          url={shareUrl} 
        >
          <LinkedinIcon size={32} round />
        </LinkedinShareButton>
        <RedditShareButton
          url={shareUrl}  >
          <RedditIcon size={32} round />
        </RedditShareButton>
        <WeiboShareButton
          url={shareUrl}
          >
          <WeiboIcon size={32} round />
        </WeiboShareButton>
       
        <Alert header="Copied to clipboard"  mess={`${shareUrl}`} handleShare={handleShare} >
        <span   className=" cursor-pointer w-8 h-8 bg-gray-700 flex justify-center items-center rounded-full">
            <LazyImagesUnStyled className=" object-contain" imageHolder={parseImageUrl('share.png')} src={parseImageUrl('share.png')}></LazyImagesUnStyled>
        </span>
        </Alert>
</div>

}