import { Carousel } from "@material-tailwind/react";
import { twMerge } from "tailwind-merge";
 
export function CarouselTransition({children,className}:{
    className?:string
    children: React.ReactNode
}) {
  return (
    <Carousel transition={{ duration: 0.5 }} className={twMerge("rounded-xl", className)}>
        {children}
    </Carousel>
  );
}