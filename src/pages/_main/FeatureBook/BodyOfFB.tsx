import { _axios_search } from "../../../axios_config"
import BookCard, { BookLoading } from "../../../lib/card/BookCard"
import { TSort, useBooksSearch } from "../../_functional/search/api"
import { Docs } from "../../_functional/search/type.api"
import { Loading } from "../../book-detail/_asset/Author"




export default function BodyofFeatured({type,limit}:{
type: TSort
limit? :number
}){

    const { isLoading , data } = useBooksSearch(`featured_${type}`,{ text:`${type ==="new" ? `first_publish_year:[2019 TO ${ new Date().getFullYear() }]`: "language:eng"}`,sort:type , limit:limit ||4})
     
    if(isLoading){
        return <div  className={`  w-full  ${limit!==1?"grid grid-cols-4":"grid grid-cols-1"} grid-rows-1  gap-7 `}>
            { BookLoading({limit:limit ? limit : 4})}
        </div>
         
    }
    if(!data?.docs){
        return Loading("error")
    }
    return <div className={`  w-full  ${limit!==1?"grid grid-cols-4":"grid grid-cols-1"} grid-rows-1  gap-7 `}>
            {
                data?.docs.map((item:Docs)=>{
                   
                    return <BookCard  type="mainpage" key={item?.key} item={item}></BookCard>
                })
            }
    </div>
}