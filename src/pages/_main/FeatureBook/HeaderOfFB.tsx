import { useTranslation } from "react-i18next"
import { Text } from "../../../lib/text"
import { TSort } from "../../_functional/search/api"
import { Link } from "react-router-dom"

export default function HeaderOfFB({type}:{
    type: TSort
}){

    const {t} = useTranslation('home')
   

    return <div className=" w-full items-end flex justify-between"> 
            <Text.S_32_500>
                {t(`featured.${type}`)}
            </Text.S_32_500>
            <Link to={`/search?q=language:eng&limit=25&page=1&sort=${type}`} className=" ">
            <Text._14_400 color=" " className=" hover:underline-offset-1 cursor-pointer hover:underline text-red-500">
            {t(`featured.button_${type}`)}
            </Text._14_400>
            </Link>
    </div>
}