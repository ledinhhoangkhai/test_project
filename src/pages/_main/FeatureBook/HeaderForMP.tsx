import { useTranslation } from "react-i18next"
import { Text } from "../../../lib/text"
import { TSort } from "../../_functional/search/api"
 
import { SparklesIcon } from "@heroicons/react/24/outline"
 

export default function HeaderForMP({type}:{
    type: TSort
}){

    const {t} = useTranslation('home')


    return <div className=" w-full items-end flex justify-between"> 
            <Text._18_600 color={ " "} className=" text-yellow animate-pulse flex gap-2 items-center">
                {t(`featured.${type}`)} <SparklesIcon className="animate-pulse self-start w-10 h-10 fill-yellow text-yellow"></SparklesIcon>
            </Text._18_600>
 
    </div>
}