import {   useTranslation } from "react-i18next"
import { LazyImagesUnStyled } from "../../../lib/React-Lazy-Image/LazyImage"
import { Text } from "../../../lib/text"
import { TNavbarItem } from "../../../i18n/type/type.nav"
import { Link } from "react-router-dom"
 
import { HomeIcon, MagnifyingGlassIcon } from "@heroicons/react/24/outline"
 
 
 




export default function Navbar({}) {
    const { t } = useTranslation('nav')
     
    const navTranslation: TNavbarItem[] = t('nav',{returnObjects:true})   
    

    return  <div className=" h-auto absolute w-full">

     <nav  className=" z-50   sticky top-0 w-full  gap-9 justify-between flex   items-center  mb:-[60px] tab:-[80px]  h-[100px] px-8 ">
            {
                    <Link  to={'/'} className="flex   gap-1 items-end border-2 border-black px-4 mb:py-1  py-2 rounded-[40rem]">
                    <LazyImagesUnStyled   className={` object-contain mb:w-8 mb:h-8  tab:w-12 tab:h-12 object-center w-12 h-12`} src={ t('nav.0.sm')} ></LazyImagesUnStyled>
                        <Text.S_32_500>
                        { t('nav.0.lg')}
                    </Text.S_32_500>
                    </Link >
            }
           <div className=" flex tab:w-1/3 mb:w-1/2 w-[20%]  justify-between">
           {
                    navTranslation.map((item)=>{
                        if(item.type ==="logo") return
                        return <Link to={item.link} className={" col-span-1 flex justify-center items-center "}>
                          { item.link ==="/" && <HomeIcon className=" lap:hidden tab:hidden rounded-[40rem] px-4 py-1 w-auto bg-white  h-8"></HomeIcon>}
                          { item.link ==="/search" && <MagnifyingGlassIcon className="lap:hidden tab:hidden rounded-[40rem] px-4 py-1 w-auto bg-white   h-8"></MagnifyingGlassIcon>}
                            <Text.P_22_300  className="tab:flex mb:hidden" >
                             {item.lg}
                            </Text.P_22_300> 
              
                        </Link>
                    })
            }
                
            </div> 
         
          
          
    </nav>
    </div> 
}  