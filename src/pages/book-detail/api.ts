import { useQuery } from "react-query";
import { _axios_lccn, _axios_lib, _axios_lib_base, fetchConfig } from "../../axios_config";
 
 



export const useFindOneBook = (id:string) => useQuery(["book_detail",id],{

    queryFn: async ()=>{
        const res = await _axios_lib.get(`${id}.json`)

        return res.data  
    },
    ...fetchConfig
}) 

export const useFindByLccn = (id:string) => useQuery(["book_lccn",id],{

    queryFn: async ()=>{
        const res = await _axios_lccn.get(`${id}.json`)

        return res.data    
    },
    ...fetchConfig,
  
}) 
export const useGetBookRating = (id:string) => useQuery(["book_rating",id],{

    queryFn: async ()=>{
        const res = await _axios_lib.get(`${id}/ratings.json`)

        return res.data  
    }
})

export const useGetBookShelves = (id:string) => useQuery(["book_shelves",id],{

    queryFn: async ()=>{
        const res = await _axios_lib.get(`${id}/bookshelves.json`)

        return res.data  
    }
})

export const useGetBookEditions = (id:string) => useQuery(["book_edition",id],{

    queryFn: async ()=>{
        const res = await _axios_lib.get(`${id}/editions.json`)

        return res.data  
    }
})

export const useBaseLibQuery = (key:string,url:string) => useQuery([`base_${key}`,url],{

    queryFn: async ()=>{
        const res = await _axios_lib_base.get(`${url}.json`)

        return res.data  
    },
    ...fetchConfig
})
