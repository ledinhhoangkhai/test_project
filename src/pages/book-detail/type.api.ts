 
 
type Tkey = {
    key: string}

export type TAuthor = {
        author:Tkey
}

export type TBookDetail={
    description:string
    authors: [TAuthor]
    covers:[number]
    dewey_number:[number]
    first_publish_date:string
    key:string
    last_modified:{
        value:string
    }
    latest_revision:number
    revision:number
    subjects:[string]
    title:string

}

export type TBookRatings ={
    summary:{
        average: number
        count: number
    }
}
export type TBookShelves ={
    counts:{     
    want_to_read:number
    currently_reading:number
    already_read:number
    }
}

export type TLccn = {
    authors:  [Tkey]
    number_of_pages: number
    by_statement:string
    covers:[string]
    key:string
    publishers:[string]
    works: [Tkey]
    latest_revision:string
    publish_date:string
}


export type TAuhtorInfo = {
    birth_date:string
    name: string
    photos:[number]
    alternate_names:[string]
}