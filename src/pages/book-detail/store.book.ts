import { create } from "zustand";
import { Docs } from "../_functional/search/type.api";
 
type BookStore ={
    cache: Docs |null,
    setCache: (cache:Docs)=> void
   
}
 
 


export const useBookStore = create<BookStore>((set)=>({
    cache:null,
    setCache: (cache) => set(()=>({cache:cache })),
}))