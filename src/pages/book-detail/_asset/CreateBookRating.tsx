import { Rating } from "@material-tailwind/react"
import { useParams } from "../../../router"
import { useGetBookRating, useGetBookShelves } from "../api"
 
import { Text } from "../../../lib/text"
 

export default function BookRating({isLoading}:{
    isLoading:Boolean
}){
    const { id} = useParams('/book-detail/:id/:lccn')
    const { data,isLoading:isRatingLoading } = useGetBookRating(id)
    const { data:Shelvesdata,isLoading:ShelvesLoading} = useGetBookShelves(id)
  
    
 
   
    if(isLoading){
        return  RatingLoading()
      }
   
   if(isRatingLoading){
    return  RatingLoading()
   }
    
    if(ShelvesLoading){
       return RatingLoading()
    }
  
    const handleRenderValue = () =>{
        if(data?.summary?.count < 1){
            return{
                value: 0,
                text: 0
            }
        }
      
        return { 
            value: parseInt(`${data?.summary?.average}`),
            text:data?.summary?.average?.toFixed(2)
        }
    }
     
    return  <>
        <Text._14_400 className=" border    items-center flex px-4 py-2 gap-2 rounded-[40rem] border-yellow">
        <Rating   className=" text-yellow"   value={  handleRenderValue().value } readonly></Rating>  |  { handleRenderValue().text}
        </Text._14_400>
            /
        <Text._14_400>
            {data?.summary.count} ratings
        </Text._14_400>
         /
        <Text._14_400>
            {Shelvesdata?.counts?.want_to_read} Want to read
        </Text._14_400>
        /
        <Text._14_400>
            {Shelvesdata?.counts?.currently_reading} Currently reading
        </Text._14_400>
        /
        <Text._14_400>
            {Shelvesdata?.counts?.already_read} Have read
        </Text._14_400>
    </>
}

 export const RatingLoading= () =>{
    return  <>
    <Text._14_400 className=" border   w-40 h-8 animate-pulse bg-blue-gray-300  items-center flex px-4 py-2 gap-2 rounded-[40rem] ">
     
    </Text._14_400>
        /
    <Text._14_400 className=" loading bullet">
         ratings
    </Text._14_400>
     /
    <Text._14_400 className=" loading bullet">
         Want to read
    </Text._14_400>
    /
    <Text._14_400 className=" loading bullet">
       Currently reading
    </Text._14_400>
    /
    <Text._14_400 className=" loading bullet">
         Have read
    </Text._14_400>
</>
 }