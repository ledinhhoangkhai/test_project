 
import { parseAuthoPhotoUrl } from "../../../axios_config"
import { LazyImagesUnStyled } from "../../../lib/React-Lazy-Image/LazyImage"
import LccnCard from "../../../lib/card/LccnCard"
import { Text } from "../../../lib/text"
import ListText from "../../../lib/text/ListText"
import { useBaseLibQuery } from "../api"
import {  TAuhtorInfo, TLccn } from "../type.api"


const LccnSample:TLccn ={
    authors:  [{
        key:"Unknowm"
    }],
    by_statement:"Unknowm",
    covers:["loading"],
    key:"Unknowm",
    publishers:["Unknowm"],
    works: [{
        key:"loading"
    }],
    latest_revision:"Unknowm",
    publish_date:"Unknowm",
    number_of_pages: 0
}


export default function Author({Lccn = LccnSample  }:{

    Lccn: TLccn
  
}){
     
    
    const {isLoading,data } = useBaseLibQuery("author",Lccn?.authors[0]?.key )
 
    const auhtor:TAuhtorInfo = data
  
    if(  isLoading){
        return Loading("loading")
    }
 
    return <>
  
        
        <LazyImagesUnStyled className="   min-w-[200px] h-[200px] rounded-md" imageHolder={auhtor?.photos ?  parseAuthoPhotoUrl(auhtor?.photos[0],'L'):""} src={ auhtor?.photos ?  parseAuthoPhotoUrl(auhtor?.photos[0],'L'):""}></LazyImagesUnStyled>
        <div className="  flex flex-col gap-2  w-full">
                <Text.S_32_500 className=" first-letter:text-56px ">
                  By  ( {auhtor?.name|| "unkown"}   {auhtor?.birth_date})
                </Text.S_32_500>
                <Text._18_600 className="   font-300 lap:font-light italic">
                   {auhtor?.alternate_names&&`OR ( ${auhtor?.alternate_names} )`    }
                </Text._18_600>

                <div className=" grid grid-cols-4 gap-3 w-full" >
                        <LccnCard>
                            <ListText title="Publish Date" content={Lccn?.publish_date}></ListText>
                        </LccnCard>
                        <LccnCard>
                            <ListText title="publishers" content={Lccn?.publishers}></ListText>
                        </LccnCard>
                        <LccnCard>
                            <ListText title="pages" content={Lccn?.number_of_pages}></ListText>
                        </LccnCard>
                        <LccnCard>
                            <ListText title="Lasted" content={Lccn?.latest_revision}></ListText>
                        </LccnCard>
                </div>
                
        </div>
    </>

    
}



export const Loading = (state:"loading" | "error",mess?:string) =>{
    
    return<> 
    <span className= { " min-w-[200px] h-[200px]   " +  state}  ></span>
    <div className="  flex flex-col gap-2  w-full">
            <Text.S_32_500 className={state+ "   first-letter:text-56px "}>
             {mess}
            </Text.S_32_500>
            <Text._18_600 className={ "     font-300 lap:font-light italic " +state }>
                 { }
            </Text._18_600>
            <div className=" grid grid-cols-4 gap-3 w-full" >
                    <LccnCard className={" border-none h-auto aspect-square w-full   " +state}>{ } </LccnCard>
                    <LccnCard className={" border-none h-auto aspect-square w-full   " +state}>{ } </LccnCard>
                    <LccnCard className={" border-none h-auto aspect-square w-full   " +state}>{ } </LccnCard>
                    <LccnCard className={" border-none h-auto aspect-square w-full   " +state}>{ } </LccnCard>
            </div>
            
    </div>
</>
}

 