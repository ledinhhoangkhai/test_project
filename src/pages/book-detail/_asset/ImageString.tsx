 
import { parseArrayCoverUrl } from "../../../axios_config"
import { LazyImagesUnStyled } from "../../../lib/React-Lazy-Image/LazyImage"
import { TBookDetail } from "../type.api"
 
import { parseImageUrl } from "../../../lib/common/getPublicImage"
import { CarouselTransition } from "../../_functional/Slider/BookCardSlider"
 
 

export default function ImageString({item,isLoading =true }:{
    item : TBookDetail
    isLoading : Boolean
}){
        

    if(isLoading) return <div className="  w-full pl-8 mb:pt-24 mb:pl-0 flex pt-36">
                <div className="mb:w-full  h-[75vh] w-full loading">

                </div>
                </div>

       
    if(!item?.covers){
        return <div className=" w-full mb:pl-0 pl-8 mb:pt-24 flex pt-36">
        <LazyImagesUnStyled imageHolder={parseImageUrl('error_image.png')} className=" rounded-lg object-cover mb:w-full   h-[75vh] min-min-w-[30rem]" src={'/'}></LazyImagesUnStyled>
        </div>

    }
    return <div  className=" w-full lap:sticky -top-2   min-h-screen gap-5 mb:pl-0 mb:min-h-screen pl-8  flex pt-36  relative   mb:pt-24  overflow-auto">
            <CarouselTransition className=" self-center    mb:w-full border-2 border-black">

            {item?.covers.map((item)=>{
            return <LazyImagesUnStyled className="    rounded-lg object-cover   h-full     w-auto" imageHolder={parseArrayCoverUrl(item,'S')} src={ parseArrayCoverUrl(item,'L') }></LazyImagesUnStyled>
            })
            }
            </CarouselTransition>
 
      
            
      
    </div>
}