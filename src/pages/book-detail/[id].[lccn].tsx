import { Text } from "../../lib/text"
 
import { useParams } from "../../router"
import ImageString from "./_asset/ImageString"
import { useFindByLccn, useFindOneBook } from "./api"
import BookRating from "./_asset/CreateBookRating";
import Author, { Loading } from "./_asset/Author"
import HeaderOfFB from "../_main/FeatureBook/HeaderOfFB";
import BodyofFeatured from "../_main/FeatureBook/BodyOfFB";
import {   useLayoutEffect } from "react";

export default function BookDetail(){
        const {id ,lccn } = useParams('/book-detail/:id/:lccn')
        const { isLoading , data  } = useFindOneBook(id)
        const {  data:BookLccn , isLoading:LccnLoading  } = useFindByLccn(lccn)
        useLayoutEffect(()=>{
                window.scrollTo(0,0)
        },[])
   
        const renderAuthor = () =>{
                       
                        if(isLoading) {

                                return Loading('loading')
                        }
                        if(LccnLoading) {

                                return Loading('loading')
                        }
                        if(!BookLccn?.authors){
                                return Loading("error","Unknown author")
                        }
                        return  <Author   Lccn={BookLccn} ></Author>
        }
    return<>
        <div className=" w-full min-h-screen mb:flex-col  pb-[100px]  flex gap-5">
                <div className="  w-[40%] mb:w-full">
                        <ImageString isLoading={isLoading} item={data}></ImageString>
                </div>
                <div className=" min-h-screen w-[60%]  mb:w-full mb:px-8 mb:pt-2    pt-36 pr-8">
                        <div className="  w-full flex flex-col gap-5">
                                 <Text._64_500 className={isLoading?" loading ":""}>
                                    {data?.title || "Loading"}
                                 </Text._64_500>
                                 <div className=" flex gap-2 items-center">
                                  <BookRating isLoading={isLoading}></BookRating>
                                 </div>
                                 <div className="  flex  gap-5 w-full min-h-[200px]">
                                         {renderAuthor()}
                                 </div>
                                <Text.P_22_300 className=" lap:font-300 break-words">
                                    { typeof data?.description==="object" ? data?.description?.value : data?.description  }
                                </Text.P_22_300>
                        </div>
                </div>
        </div>
       {LccnLoading? null:LccnLoading? null :  <div key={"detail"+"random"} className=" gap-[100px] tab:gap-[70px] mb:gap-[50px] min-h-screen pb-[100px] w-full flex flex-col items-center px-8">
                    <HeaderOfFB type={"random"}></HeaderOfFB>
              
                    <BodyofFeatured limit={8} type={"random"}></BodyofFeatured>
                
                    
        </div>    }
    </> 
}