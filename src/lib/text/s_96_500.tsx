import React, { useLayoutEffect, useRef } from "react";
import { twMerge } from "tailwind-merge";
import { black } from "../../assets/color";
import gsap from "gsap";

 
export default function S_96_500({className,children, color = black,text}:{
    className?: string
    children: React.ReactNode | string
    color?: string
    text? :string
   
}){       let textRef = useRef(null)
        useLayoutEffect(()=>{
 
                let ctx = gsap.context(() => {
                    if(text){
                        let tl = gsap.timeline({ yoyo:true , repeatDelay:1 ,repeat: 999})
                        tl.to('.typing',{
                            text:' ',
                           duration:2,   
                        })
                        tl.to(".typing",{
                           text:text,
                           duration:2,    
                         
                        })   
                    }
                    
                  }, textRef);
           
              
              return () => ctx.revert(); 
        },[])

     
      return    <span style={{
        color: color
    }} ref={textRef} className={twMerge("lap:text-96 mb:text-56px lap:font-medium tracking-tighter  ",className)}>
        {children}
    </span>
    
}