import { Text } from ".";

export default function ListText({title,content}:{
    title :string
    content: string | [string] | number
}){


    return <span className=" flex flex-col   h-auto">
        <Text._18_600 className="  capitalize text-center">
            {title}
        </Text._18_600>
        <Text._14_400 className=" flex flex-col  text-center italic lap:font-light">
            {content}
        </Text._14_400>
    </span>
}