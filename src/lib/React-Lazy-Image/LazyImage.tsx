
import { useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { parseImageUrl } from '../common/getPublicImage';
export default function LazyImages({src,className,alt}:{
  
    className?: string 
    src :string | undefined
    alt?:string
   
}) {
    
    return <LazyLoadImage
    className={className}
    effect="blur"
     style={{
       
        height:"100%",
        width:"100%"
     }}
    src={src}
    alt={alt}
    >  
    </LazyLoadImage>
}
export const   LazyImagesUnStyled= ({src,className,alt,imageHolder}:{
  
    className?: string 
    src :string | undefined
    alt?:string
 
    imageHolder?: string
}) => {
    const  [_src,setSrc] = useState(src)
    return <div className={className + " relative flex items-center justify-center"}>
     {  imageHolder &&  <div style={{
        backgroundImage:`url(${imageHolder})`,
        backgroundRepeat: "no-repeat",
        backgroundSize:"cover",
        backgroundPosition:"center",
        opacity:0.8,
    
    }} className=' w-full h-full absolute'>

    </div>}
 <LazyLoadImage
    
    className={className  +  (imageHolder ?  " scale-90  shadow-2xl   rotate-6 ": "")}
    effect="blur"
    style={{
      objectFit:"contain",
       
    }}
    src={_src}
    alt={alt}
    onError={()=>setSrc( parseImageUrl('error_image.png')) }
    loading='eager'
    >  
   
    </LazyLoadImage>
    </div>
    
}