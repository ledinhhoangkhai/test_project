 
import { Button, IconButton } from "@material-tailwind/react";
import { ArrowRightIcon, ArrowLeftIcon } from "@heroicons/react/24/outline";
 
import { useSearchParams } from "react-router-dom";
import { useSearchStore } from "../../pages/_functional/search/api.store";
 
 
export function DefaultPagination({isLoading}:{
    isLoading?: Boolean
} ) {
 
  const [q,setQ]= useSearchParams()
    const  data = useSearchStore( state => state.data)
    let current = parseInt(q.get('page')|| "0") 
    let limit = parseInt(q.get('limit')|| "0") 
    
  const getItemProps = (index:number) =>
    ({
      variant: current === index ? "filled" : "text",
      color: "gray",
      onClick: () =>{
        if(isLoading) return
        if(!data?.docs.length ) return
        return setQ( prev =>{
        
        prev.set('page', current.toString())
        return prev
     },{replace:true})},
    } as any);
 
  const next = () => {
    if(isLoading) return
    if(!data?.docs.length ) return
    if (data?.docs.length < limit) return;
 
    setQ( prev =>{
        prev.set('page',( current +1).toString())
        return prev
     },{replace:true})
  };
 
  const prev = () => {
    if(isLoading) return
   
    if (current === 1) return;
 
    setQ( prev =>{
        prev.set('page',( current -1).toString())
        return prev
     },{replace:true})
  };
 
  return (
    <div className="flex items-center gap-4 text-18">
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={prev}
        disabled={current === 1}
      >
        <ArrowLeftIcon  strokeWidth={2} className="h-4 w-4" />
        <span className=" mb:hidden tab:inline-block"> Previous</span>
      </Button>
      <div className="flex items-center gap-2">
        
      { current-1 > 0  && <IconButton {...getItemProps(current-1)}>{current-1}</IconButton>}
        <IconButton {...getItemProps(current)}>{current}</IconButton>
        <IconButton {...getItemProps(current+1)}>{current+1}</IconButton>
        ..........
        <IconButton {...getItemProps(current+3)}>{current+3}</IconButton>
      </div>
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={next}
        disabled={!data?.docs.length ? true : data?.docs.length < limit ? true : false}
      >
        <span className=" mb:hidden tab:inline-block">Next</span>
        
        <ArrowRightIcon   strokeWidth={2} className="h-4 w-4" />
      </Button>
    </div>
  );
}