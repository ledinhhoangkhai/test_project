import { twMerge } from "tailwind-merge"

 





export default function LccnCard({children,className}:{
    children: React.ReactNode
    className?:string
}){
    return <span className={ twMerge("  h-auto items-start py-5 flex   justify-center border-black border w-full rounded-xl",className)}>
                {children}
    </span>
}