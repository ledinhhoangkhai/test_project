import { useTranslation } from "react-i18next"
import { parseCoverUrl } from "../../axios_config"
import { Docs } from "../../pages/_functional/search/type.api"
import   { LazyImagesUnStyled } from "../React-Lazy-Image/LazyImage"
import { Text } from "../text"
import { parseImageUrl } from "../common/getPublicImage"
import { Link } from "../../router"
import { getOLID } from "../common/handleParse"
import { twMerge } from "tailwind-merge"
import { white } from "../../assets/color"
import { Spinner } from "@material-tailwind/react"
import Share from "../../pages/_functional/share/Share"


const loadingItem: Docs = {
    already_read_count: 10,
    author_alternative_name: ["John Doe", "J. D."],
    author_facet: ["Doe, John"],
    author_key: ["123456789"],
    author_name: ["John Doe"],
    contributor: ["Jane Smith"],
    cover_edition_key: "abcdef123456",
    cover_i: 1,
    currently_reading_count: 5,
    ebook_count_i: 3,
    edition_count: 2,
    edition_key: ["edition1", "edition2"],
    first_publish_year: 2000,
    first_sentence: ["Once upon a time..."],
    isbn: ["9781234567890"],
    key: "book123",
    language: ["English"],
    last_modified_i: 1630000000,
    publish_date: ["2021-01-01"],
    lccn: null,
    publish_place: ["New York"],
    publish_year: [2021],
    publisher: ["ABC Publishing"],
    ratings_average: 4.5,
    ratings_count: 100,
    ratings_count_1: 10,
    ratings_count_2: 20,
    ratings_count_3: 30,
    ratings_count_4: 25,
    ratings_count_5: 15,
    ratings_sortable: 4.2,
    readinglog_count: 50,
    seed: ["seed123"],
    subject: ["Fiction", "Mystery"],
    subject_facet: ["Fiction", "Mystery"],
    subject_key: ["fiction", "mystery"],
    title: "This titel is loading, please wait !!! ",
    title_sort: "Sample Book",
    title_suggest: "Sample Book",
    type: "book",
    want_to_read_count: 20,
}


export default function BookCard({item=loadingItem,className,color,isLoading =false,type}:{
    item?: Docs
    className?:string
    color? :string
    isLoading?:Boolean
    type?:  undefined | "mainpage"
}){
     const {t} = useTranslation('home')
    if(isLoading){
        return <span className={twMerge(" flex flex-col h-[75vh] justify-between",className)}>
        

        <span className=" w-full animate-pulse flex justify-center items-center bg-blue-gray-300  rounded-md  h-[55vh]" >
            <Spinner></Spinner>
        </span>
       <div className=" w-full items-start flex  flex-col ">

        <Text._18_600 color ={' '} className="rounded-t-xl rounded-e-xl w-full  line-clamp-2 text-blue-gray-300    animate-pulse bg-blue-gray-300    lap:font-500 leading-[1.2]  ">
        {item?.title}
 
        </Text._18_600>
        <Text._18_600 color ={' '} className="rounded-b-xl   px-2   line-clamp-2 text-blue-gray-300    animate-pulse bg-blue-gray-300    lap:font-500 leading-[1.2]  ">
        {item?.title_sort}
        </Text._18_600>
      <Text._18_600 color ={' '} className=" pt-2  text-blue-gray-300   animate-pulse   line-clamp-1  capitalize">
          <span className="   rounded-xl bg-blue-gray-300 ">{t('featured._author')}</span> :  <Text._14_400 className="rounded-xl bg-blue-gray-300 " color ={' '}>{  item?.author_name[0]  }</Text._14_400> 
        </Text._18_600>
        <Text._14_400 color ={' '} className="capitalize text-blue-gray-300  animate-pulse  italic">
          <span className=" rounded-xl px-2 bg-blue-gray-300">  {t('featured._publish')}</span> : <span className=" rounded-xl px-2 bg-blue-gray-300"> 2023</span> 
        </Text._14_400>
       </div>
        <Text._18_600 color ={color} className=" flex items-center justify-center gap-2 ">
            <span style={{
                borderColor:color,
                background:color
            }} className=" border flex-grow bg-blue-gray-300 rounded-xl border-blue-gray-300 h-1"></span> 
            <span className=" px-6 rounded-xl text-blue-gray-300 py-1 self-center gap-2 flex items-center animate-pulse bg-blue-gray-300 ">   { "999" } </span>
            <span  style={{
                borderColor:color,
                background:color
            }} className="flex-grow border bg-blue-gray-300 rounded-xl border-blue-gray-300 h-1"></span>
        </Text._18_600>
           
        </span>
    }  
  
    let id =  getOLID(item?.key)
  
    const lccn = Array.isArray( item?.lccn) ? item?.lccn[0]: 'un' 
    return <div  className={ twMerge(   ` ${type && " bg-gray-50"} tab:col-span-2 mb:col-span-4 border-2 border-black px-2 py-2 rounded-lg w-full h-auto`,className)}>
        
            <Link onClick={(e)=>{
                if(item?.key ==="404"){
                    return   e.preventDefault()
                 } 
                 window.scrollTo(0,0)
                return e }}  params={{ id :id, lccn:lccn }} to={'/book-detail/:id/:lccn'} className={  " flex flex-col h-[70vh] justify-between"}>
            
            <LazyImagesUnStyled className=" w-full rounded-md hover:object-cover  h-[50vh]" src={parseCoverUrl(item,'M')} imageHolder={parseCoverUrl(item,'S')} ></LazyImagesUnStyled>
           <div className=" w-full items-start flex flex-col ">

            <Text._18_600 color ={color} className=" line-clamp-2    lap:font-500 leading-[1.2]  ">
            {item?.title}
            </Text._18_600>
           {item?.author_name  ? <Text._18_600 color ={color} className="line-clamp-1  capitalize">
               {t('featured._author')}:  <Text._14_400 color ={color}>{  item?.author_name[0]  }</Text._14_400> 
            </Text._18_600>: <span className=" opacity-0">
                No Auhor
                </span>}
            <Text._14_400 color ={color} className="capitalize italic">
                {t('featured._publish')}: { item?.publish_year ? Math.max(...item?.publish_year): "unknown"}
            </Text._14_400>
           </div>
            <Text._18_600 color ={color} className=" flex items-center justify-center gap-2 ">
                <span style={{
                    borderColor:color,
                    background:color
                }} className=" border flex-grow bg-black rounded-xl border-black h-1"></span> 
                <span className=" self-center gap-2 flex items-center"> <LazyImagesUnStyled imageHolder={parseImageUrl('read.png')}   className="   w-5 h-5" src={color === white ? parseImageUrl('read_white.png')  :parseImageUrl('read.png')} />  {item?.readinglog_count ? item?.readinglog_count :"0" } </span>
                <span  style={{
                    borderColor:color,
                    background:color
                }} className="flex-grow border bg-black rounded-xl border-black h-1"></span>
            </Text._18_600>
            </Link>
        {item?.key!=="404" &&    <Share uri={`/book-detail/${id}/${lccn}`} ></Share>}
    </div>
}

export const  BookLoading  =({limit=4}) =>{
            
    const loadingSample = Array.from({ length: limit }, () =>  Math.random());
   
    return loadingSample.map((item)=>{
       return <BookCard  className=" mb:col-span-4 tab:col-span-2" key={item} isLoading={true} ></BookCard>
  })
}

 
 