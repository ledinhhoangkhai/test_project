import { useEffect, useState } from "react"
import { TParams } from "../../pages/_functional/search/type.api"

export const useDebounceSearch =  (  value:TParams , delay=500)=>{
    const [q,setQ] = useState(value)

    useEffect(()=>{
        const timeout = setTimeout(()=>{
            setQ(value)
        },delay)

        return () => clearTimeout(timeout)
    },[value.text,value.page,value.limit,value.sort ])
    return q
}