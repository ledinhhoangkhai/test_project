export type TNavbarItem = {
    type:string,
    lg:string,
    link:string ,
    sm:string
}