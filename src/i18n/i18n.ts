import i18next from "i18next";
import { useTranslation } from "react-i18next";
import { initReactI18next} from "react-i18next"
import  HOME_TW from './zh-Hant-TW/home.json'
import NAV_TW from './zh-Hant-TW/nav.json'
import HOME_EN from './en/home.json'
import NAV_EN from './en/nav.json'
 
export type pages = 'home' | 'collections'
const resources ={
    en:{
        home: HOME_EN,
        nav:  NAV_EN
    },
    zh_Hant_TW:{
        home:  HOME_TW,
        nav: NAV_TW
    }
}

const defaultNS = 'home'
i18next.use(initReactI18next).init({
    resources,
    lng:'en',
    ns:['home','nav'],
    fallbackLng: 'en',
    defaultNS,
    interpolation :{
        escapeValue: false
    },
   
})



export const useTransHome = (pages: pages  , array:boolean=false ) =>{
   const {t } =    useTranslation("home")
    if(array){
        return    { 
        
            _t:  (string:any,index?:any)=> t(`${pages}.${index}.${string}`)
          
          }
    }

    return{ 
        
      _t:  (string:any)=> t(`${pages}.${string}`)
    
    }

}


export const useApiTrans = () =>{
    const {i18n } =    useTranslation()
    
    return{ 
      t :  ( obj: any) => obj[i18n.language] }
}

 