import axios from "axios";
import { Docs } from "./pages/_functional/search/type.api";
 

export const _axios_strapi = axios.create({
    baseURL: import.meta.env.VITE_STRAPI_API_URL,
    headers:{
      Authorization: "bearer " +import.meta.env.VITE_REACT_APP_API_TOKEN
    },
    timeout: 1000,
  });
export const coverUrl=  import.meta.env.VITE_COVER_API
export type TImageSize = 'S' |'M'|'L'
export const parseCoverUrl= (key:Docs,size:TImageSize)=>{
  if(!key.cover_i){
    return ''
  }
 
  return `${coverUrl}${key.cover_i}-${size}.jpg`
}
export const parseAuthoPhotoUrl= (key:number,size:TImageSize)=>{
 
    return `${coverUrl}${key}-${size}.jpg`
  
}
export const parseArrayCoverUrl= (key:number,size:TImageSize)=>{

  return `${coverUrl}${key}-${size}.jpg`
}
export const _axios_lccn = axios.create({
  baseURL: import.meta.env.VITE_LCCN_API_URL,
  timeout: 100000,
})
export const _axios_lib = axios.create({
    baseURL: import.meta.env.VITE_LIB_API_URL,
  
    timeout: 100000,
  });

export const _axios_lib_base = axios.create({
    baseURL: import.meta.env.VITE_LIB_API_BASE,
  
    timeout: 100000,
  });
  export const _axios_search = axios.create({
    baseURL: import.meta.env.VITE_LIB_API_SEARCH,
    timeout: 1000000,
  });
  export const fetchConfig ={
    refetchOnMount:false,
    refetchInterval:Infinity,
    staleTime: Infinity,
    refetchOnWindowFocus:false
  }

